import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
import TypeNav from './components/TypeNav'
import store from './store'
import './mock/mockServer'
import Carousel from './components/Carousel'
import Pagination from './components/Pagination'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as API from './api'


Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()
Vue.prototype.$api = API
Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.component('TypeNav',TypeNav)
Vue.component('Carousel',Carousel)
Vue.component('Pagination',Pagination)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')

