import {nanoid} from 'nanoid'

export const uuid = ()=>{
    let data = localStorage.getItem('uuid')
    if (!data) {
        localStorage.setItem('uuid',nanoid())
    }
    
}