import {reqGetCode,reqRegister,reqLogin, reqUserInfo,reqLogout} from '../../../api'

export default{
    namespaced:'user',
    actions:{
        async getCode(context,iphone){
            let result = await reqGetCode(iphone)
            if (result.code == 200) {
                context.commit('GETCODE',result.data)
                return 'ok'
            }else{
                return Promise.reject(new Error('fail'))
            }
        },

        async completed(context,data){
            let result = await reqRegister(data)
            if (result.code == 200) {
                return 'ok'
            }
            else{
                return Promise.reject(result.message)
            }
        },

        async userLogin(context,data){
            let result = await reqLogin(data)
            if (result.code == 200) {
                localStorage.setItem('TOKEN',result.data.token)
                return 'ok'
            }else{
                return  Promise.reject(result.message) 
            }
        },

        async getUserInfo(context){
            let result = await reqUserInfo()
            if (result.code == 200) {
                context.commit('GETUSERINFO',result.data)
            }else{
                return Promise.reject(new Error('fail'))
            }
            
        },

        async userLogout(context){
            let result = await reqLogout()
            if (result.code == 200) {
                context.commit('USERLOGOUT')
                return 'ok'
            }else{
                return Promise.reject(new Error('fail'))
            }
        }
    },
    mutations:{
        GETCODE(context,code){
            context.codeNum = code
        },
        
        GETUSERINFO(context,value){
            context.userInfo = value
        },

        USERLOGOUT(context){
            localStorage.clear('TOKEN')
            context.userInfo=''
        }
    },
    state:{
        codeNum:'',
        userInfo:{}
    }
}