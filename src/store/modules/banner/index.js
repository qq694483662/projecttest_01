import {reqBanner} from '../../../api'

export default {
    namespaced: 'bannerList',
    actions:{
        async getBannerList(context){
           var result = await reqBanner()
            if (result.code == 200) {
                context.commit('GETBANNERLIST',result.data)
            } 
        }
    },
    mutations:{
        GETBANNERLIST(context,value){
            context.bannerList=value
            
        }
    },
    state:{
        bannerList:[],
    }
}