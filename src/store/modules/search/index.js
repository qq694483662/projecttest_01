import {reqGetSearchInfo} from '../../../api'

export default {
    namespaced:'searchList',
    actions:{
        async getSerachList(context,value={}){
            let result = await reqGetSearchInfo(value)
            if (result.code == 200) {
                context.commit('GETSEARCHLIST',result.data)
            }
        }
    },
    mutations:{
        GETSEARCHLIST(context,value){
            context.searchList = value
        }
    },
    state:{
        searchList:{}
    }
}