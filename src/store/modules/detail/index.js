import {reGetGoodInfo,reqAddOrUpdateShopCart} from '../../../api'
import {uuid} from '../../../uuid'

export default{
    namespaced:'goodDetail',
    actions:{
       async getGoodDetail(context,value){
            let result = await reGetGoodInfo(value)
            if (result.code == 200) {
                context.commit('GETGOODDETAIL',result.data)
            }
        },

        async addOrUpdateShopCart(context,{skuId,skuNum}){
            let result =await reqAddOrUpdateShopCart(skuId,skuNum)
            if (result.code ==200) {
                return 'ok'
            }else{
                console.log(1);
                return Promise.reject(new Error('fail'))
            }
            
        }
    },
    mutations:{
      GETGOODDETAIL(context,value){
          context.goodDetail = value
        }
    },
    state:{
        goodDetail:{},
        uuid:uuid()
        
    },

    getters:{
        categoryView(state){
            return state.goodDetail.categoryView || {}
        },
        skuInfo(state){
            return state.goodDetail.skuInfo || {}
        },

    }
}