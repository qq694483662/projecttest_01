import {reqCategoryList} from '../../../api'

export default {
    namespaced: 'home',
    actions:{
        async categoryList(context){
           var result = await reqCategoryList()
            if (result.code == 200) {
                context.commit('CATEGORYLIST',result.data)
            } 
        }
    },
    mutations:{
        CATEGORYLIST(context,value){
            context.categoryList=value
            
        }
    },
    state:{
        categoryList:[],
    }
}