import {reqCartList,reqDeleteCart,reqCheckUpdate} from '../../../api'

export default{
    namespaced:'cartList',
    actions:{
        async getCartList(context){
            let result=await reqCartList()
            if (result.code == 200) {
                context.commit('GETCARTLIST',result.data)
            }
        },
        async deleteItem(context,skuId){
            let result = await reqDeleteCart(skuId)
            if (result.code == 200) {
                return 'ok'
            }else{
                return Promise.reject(new Error('fail'))
            }
        },
        allDeleteItem(context){
            let promiseAll =[]
            let arr = context.getters.cartList.cartInfoList
            console.log(context.getters.cartList.cartInfoList);
            arr.forEach(i => {
                if (i.isChecked == 1) {
                    let promise =context.dispatch('deleteItem',i.skuId)
                    promiseAll.push(promise)
                }
                
            })
            return Promise.all(promiseAll)    
        },

        async checkUpdate(context,{skuId,isChecked}){
            let result = await reqCheckUpdate(skuId,isChecked)
            if (result.code == 200) {
                return 'ok'
            }else{
                return Promise.reject(new Error('fail'))
            }
        },

        allCheck(context,value){
          let promiseAll =[]
          let arr = context.getters.cartList.cartInfoList
          arr.forEach(i => {
            let promise = context.dispatch('checkUpdate',{skuId:i.skuId,isChecked:value})
            promiseAll.push(promise)
          });
          return Promise.all(promiseAll)
        }
    },
    mutations:{
        GETCARTLIST(context,value){
            context.cartList = value
        }
    },
    state:{
        cartList:[]
    },
    getters:{
        cartList(state){
            return state.cartList[0] || {}
        }
    }
}