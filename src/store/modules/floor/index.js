import {reqFloor} from '../../../api/index'

export default{
    namespaced: 'floorList',
    actions:{
        async getFloorList(context){
            let result = await reqFloor()
            if (result.code == 200) {
                context.commit('GETFLOORLIST',result.data)
            }
           
        }

    },
    mutations:{
        GETFLOORLIST(context,value){
            context.floorList = value
        }
    },
    state:{
        floorList:[]
    }
}