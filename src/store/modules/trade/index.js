import {reqAddressInfo,reqOrderInfo} from '../../../api'

export default{
    namespaced:'trade',
    actions:{
        async getUserAddress(context){
            let result = await reqAddressInfo()
            if (result.code == 200) {
                context.commit('GETUSERADDRESS',result.data)
            }
        },

        async getOrderInfo(context){
            let result = await reqOrderInfo()
            if (result.code == 200) {
                context.commit('GETORDERINFO',result.data)
            }
        }
    },
    mutations:{
        GETUSERADDRESS(context,value){
            context.address = value
        },
        GETORDERINFO(context,value){
            context.order = value
        }
    },
    state:{
        address:[],
        order:{}
    }
}