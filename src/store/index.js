import Vue from 'vue'
import Vuex from 'vuex'
import home from './modules/home'
import bannerList from './modules/banner'
import floorList from './modules/floor'
import searchList from './modules/search'
import goodDetail from './modules/detail'
import cartList from './modules/cartList'
import user from './modules/user'
import trade from './modules/trade'


Vue.use(Vuex)
export default new Vuex.Store({
    modules:{
        home,
        bannerList,
        floorList,
        searchList,
        goodDetail,
        cartList,
        user,
        trade
    }
})