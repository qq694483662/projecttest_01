import axios from 'axios'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

var requests = axios.create({
    baseURL: '/mock'
    });

// 添加请求拦截器
requests.interceptors.request.use((config) =>{
    // 在发送请求之前做些什么
    nprogress.start()
    return config;
}),

// 添加响应拦截器
requests.interceptors.response.use((response)=> {
    nprogress.done()
    return response.data;
}, (error) =>{
    return Promise.reject(error);
})

export default requests
