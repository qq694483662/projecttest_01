import axios from './axios'
import mockAxios from './mockAxios'

//获取分类数据
export const reqCategoryList = () => {
   return axios.get('/product/getBaseCategoryList')
}

//获取轮播图
export const reqBanner = () => {
   return mockAxios.get('/banner')
}

//获取floor数据
export const reqFloor = () => {
   return mockAxios.get('/floor')
}

//发起搜索请求
export const reqGetSearchInfo = (params) => {
   return axios.post('/list',params)
}

//发起商品详情请求
export const reGetGoodInfo = (sukId)=>{
   return axios.get(`/item/${sukId}`)
}

//加入商品到购物车
export const reqAddOrUpdateShopCart = (skuId,skuNum)=>{
   return axios.post(`/cart/addToCart/${skuId}/${skuNum}`)
}

//获取购物车商品
export const reqCartList = ()=>{
   return axios.get('/cart/cartList')
}

//删除购物车商品
export const reqDeleteCart = (skuId)=>{
   return axios.delete(`/cart/deleteCart/${skuId}`)
}

//勾选商品发送请求
export const reqCheckUpdate = (skuId,isChecked) =>{
   return axios.get(`/cart/checkCart/${skuId}/${isChecked}`)
}

//获取注册验证码
export const reqGetCode = (phone) => {
   return axios.get(`/user/passport/sendCode/${phone}`)
}

//完成注册
export const reqRegister = (data) =>{
   return axios.post('/user/passport/register',data)
}

//登录
export const reqLogin = (data)=>{
   return axios.post('/user/passport/login',data)
}

//获取用户信息
export const reqUserInfo = () =>{
   return axios.get('/user/passport/auth/getUserInfo')
}

//退出登录
export const reqLogout = () =>{
   return axios.get('/user/passport/logout')
}

//获取结账页面地址数据
export const reqAddressInfo = () =>{
   return axios.get('/user/userAddress/auth/findUserAddressList')
}

//获取结账页面商品数据
export const reqOrderInfo = () =>{
   return axios.get('/order/auth/trade')
}

//提交订单
export const reqSubmitOrder = (tradeNo,data) =>{
   return axios.post(`/order/auth/submitOrder?tradeNo=${tradeNo}`,data)
}

//获取支付信息
export const reqPayInfo = (orderId) =>{
   return axios.get(`/payment/weixin/createNative/${orderId}`)
}

//支付状态
export const reqPayStatus = (orderId) =>{
   return axios.get(`/payment/weixin/queryPayStatus/${orderId}`)
}

//获取个人中心我的订单
export const reqMyOrderList = (page,limit) =>{
   return axios.get(`/order/auth/${page}/${limit}`)
}