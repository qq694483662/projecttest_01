import axios from 'axios'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
// import store from '../store'

var requests = axios.create({
    baseURL: 'http://39.98.123.211/api'
    });

// 添加请求拦截器
requests.interceptors.request.use((config) =>{
    // 在发送请求之前做些什么
    config.headers.userTempId=localStorage.getItem('uuid')
    config.headers.token=localStorage.getItem('TOKEN')
    
    nprogress.start()
    return config;
}),

// 添加响应拦截器
requests.interceptors.response.use((response)=> {
    nprogress.done()
    return response.data;
}, (error) =>{
    return Promise.reject(error);
})

export default requests
