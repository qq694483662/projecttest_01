import VueRouter from 'vue-router'
import Home from '../pages/Home'
import Login from '../pages/Login'
import Register from '../pages/Register'
import Search from '../pages/Search'
import Detail from '../pages/Detail'
import AddCartSuccess from '../pages/AddCartSuccess'
import ShopCart from '../pages/ShopCart'
import store from '../store'
import Trade from '../pages/Trade'
import Pay from '../pages/Pay'
import PaySuccess from '../pages/PaySuccess'
import Center from '../pages/Center'
import MyOrder from '../pages/Center/MyOrder'

const router = new VueRouter({
    routes: [
        {   
            path:'/',
            redirect:'home',
        },
        {
            path:'/home',
            component:Home,
            meta:{
                footerIshow:true,
                TypeNavList:true
            },
        },
        {   
            path:'/login',
            component:Login
        },
        {
            path:'/register',
            component:Register
        },
        {   
            name:'search',
            path:'/search/:keyword?',
            component:Search,
            meta:{
              footerIshow:true
            },
            
        },
        {   
            name:'detail',
            path:'/detail/:goodID',
            component:Detail,
            meta:{
              footerIshow:true
            },
        },
        {
            name:'addcartsuccess',
            path:'/addcartsuccess',
            component:AddCartSuccess,
            meta:{
                footerIshow:true
            },   
        },
        {
            name:'shopcart',
            path:'/shopcart',
            component:ShopCart,
            meta:{
               footerIshow:true,
             },   
        },
        {
            name:'trade',
            path:'/trade',
            component:Trade,
            meta:{
                isLogin:false
            }
        },
        {
            name:'pay',
            path:'/pay',
            component:Pay,
            meta:{
                isLogin:false
            },
            beforeEnter:(to,from,next)=>{
                if (from.path == '/trade') {
                    next()
                }else{
                    next(false)
                }
            }
        },
        {
            name:'paysuccess',
            path:'/paysuccess',
            component:PaySuccess,
            meta:{
                isLogin:false
            },
            beforeEnter:(to,from,next)=>{
                if (from.path == '/pay') {
                    next()
                }else{
                    next(false)
                }
            }
        },
        {
            name:'center',
            path:'/center',
            component:Center,
            redirect:'/center/myorder',
            children:[
                {
                    name:'myorder',
                    path:'myorder',
                    component:MyOrder,
                    meta:{
                        isLogin:false,
                        go:true
                    },
                }
            ]
        }

        
    ],
    scrollBehavior (){
        return {y:0}
    },   
})

router.beforeEach(async (to,from,next)=>{
        if (localStorage.getItem('TOKEN')) { 
            try {
                await store.dispatch('user/getUserInfo')
                if (to.path=='/login' || to.path=='/register') {
                    next('/')
                }
                else{
                    next()
                }
            } catch {
                await store.dispatch('user/userLogout')
                next('/login')     
            }
        }
        else{
            if(to.name=='myorder' && to.meta.go==true){
                next(`/login?redirect=${to.path}`)
            }
            else if(to.meta.isLogin == false){
                next('/login')
            }
            else{
                next()
            }
            
        }
})

export default router

